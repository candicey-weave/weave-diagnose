package com.gitlab.candicey.weavediagnose

import java.io.File
import kotlin.system.exitProcess

/**
 * System properties:
 * - minecraft.directory: Minecraft directory
 * - memory.minimum: Minimum memory (default: 1536m)
 * - memory.maximum: Maximum memory (default: 1536m)
 */
object CommandBuilder {
    fun buildCommand(additionalArguments: List<String> = emptyList(), withWeave: Boolean = true): List<String> {
        val commands = mutableListOf(
            findJava().canonicalPath,
            *getArgumentsList().toTypedArray()
        )

        commands.addAll(1, additionalArguments)

        if (withWeave) {
            commands.add(1, "-javaagent:${getWeaveJarOrExit().canonicalPath}")
        }

        return commands
    }

    fun getWeaveJarOrExit(): File = WeaveHandler.getJar() ?: run {
        LoggingHandler.error("Weave jar not found. Please update Weave first.")
        exitProcess(0)
    }

    fun getArgumentsList(): List<String> =
        listOf(
            "--add-modules",
            "jdk.naming.dns",
            "--add-exports",
            "jdk.naming.dns/com.sun.jndi.dns=java.naming",
            "-Djna.boot.library.path=natives",
            "-Dlog4j2.formatMsgNoLookups=true",
            "--add-opens",
            "java.base/java.io=ALL-UNNAMED",
            "-Djava.library.path=natives",
            "-Xms${System.getProperty("memory.minimum") ?: "${Config.INSTANCE.minMemory}m"}",
            "-Xmx${System.getProperty("memory.maximum") ?: "${Config.INSTANCE.maxMemory}m"}",
            "-cp",
            getLunarJars().joinToString(if (OperatingSystem.current() == OperatingSystem.WINDOWS) ";" else ":") { it.name },
            "com.moonsworth.lunar.genesis.Genesis",
            "--version",
            "1.8.9",
            "--accessToken",
            "0",
            "--assetIndex",
            "1.8",
            "--userProperties",
            "{}",
            "--gameDir",
            findMinecraftDirectory().canonicalPath,
            "--texturesDir",
            "${findLunarDirectory()}/textures",
            "--launcherVersion",
            "3.0.6",
            "--width",
            "854",
            "--height",
            "480",
            "--workingDirectory",
            ".",
            "--classpathDir",
            ".",
            "--ichorClassPath",
            getLunarJars().joinToString(","),
            "--ichorExternalFiles",
            "OptiFine_v1_8.jar"
        )

    fun getLunarJars(): List<File> {
        val jarList = listOf(
            "lunar-lang.jar",
            "lunar-emote.jar",
            "lunar.jar",
            "optifine-0.1.0-SNAPSHOT-all.jar",
            "v1_8-0.1.0-SNAPSHOT-all.jar",
            "common-0.1.0-SNAPSHOT-all.jar",
            "genesis-0.1.0-SNAPSHOT-all.jar",
        )

        val lunarDirectory = findLunarDirectory()
        val jars = jarList.map {
            File("${lunarDirectory}/offline/multiver", it).also { jar ->
                if (!jar.exists()) {
                    System.err.println("${jar.name} not found.")
                    exitProcess(0)
                }
            }
        }

        return jars
    }

    fun findMinecraftDirectory(): File {
        return System.getProperty("minecraft.directory")?.let(::File) ?: when (OperatingSystem.current()) {
            OperatingSystem.WINDOWS -> File(System.getenv("APPDATA"), ".minecraft")
            OperatingSystem.MACOS -> File(HOME, "Library/Application Support/minecraft")
            else -> File(HOME, ".minecraft")
        }.also {
            if (!it.exists()) {
                System.err.println("Minecraft directory not found, please install it first.")
                exitProcess(0)
            }
        }
    }

    fun findLunarDirectory(): File {
        return File(HOME, ".lunarclient").also {
            if (!it.exists()) {
                System.err.println("Lunar Client directory not found, please install it first.")
                exitProcess(0)
            }
        }
    }

    fun findJava(): File {
        val lunarDirectory = findLunarDirectory()
        val jreDirectory = File(lunarDirectory, "jre").also {
            if (!it.exists()) {
                System.err.println("Lunar Client JRE directory not found.")
                exitProcess(0)
            }
        }

        val zulu = jreDirectory.listFiles()
            ?.filter { it.isDirectory }
            ?.flatMap { it.listFiles()?.toList() ?: emptyList() }
            ?.filter { it.name.startsWith("zulu") }
            ?.sortedByDescending { it.name }
            ?.firstOrNull()
            ?: run {
                System.err.println("Zulu not found.")
                exitProcess(0)
            }

        val binary = File(zulu, "bin").also {
            if (!it.exists()) {
                System.err.println("Zulu not found.")
                exitProcess(0)
            }
        }

        val java = File(binary, if (OperatingSystem.current() == OperatingSystem.WINDOWS) "java.exe" else "java").also {
            if (!it.exists()) {
                System.err.println("Java not found.")
                exitProcess(0)
            }
        }

        return java
    }
}
