package com.gitlab.candicey.weavediagnose

import java.awt.Desktop
import javax.swing.JOptionPane

object LoggingHandler {
    fun info(message: String, showGuiIfAvailable: Boolean = true) {
        println(message)
        if (showGuiIfAvailable && isGui) {
            JOptionPane.showMessageDialog(null, message, "Info", JOptionPane.INFORMATION_MESSAGE)
        }
    }

    fun error(message: String, showGuiIfAvailable: Boolean = true) {
        System.err.println(message)
        if (showGuiIfAvailable && isGui) {
            JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE)
        }
    }

    fun onGameExit(code: Int) {
        if (!isGui) {
            return
        }

        if (code != 0) {
            val options = arrayOf("Yes", "No")
            val choice = JOptionPane.showOptionDialog(null, "The game has exited unexpectedly. Would you like to open the logs?", "Weave Diagnose", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0])
            when (choice) {
                0 -> {
                    if (!LOGS_FILE.exists()) {
                        JOptionPane.showMessageDialog(null, "No logs found.")
                        return
                    }

                    Desktop.getDesktop().open(LOGS_DIRECTORY)
                }
            }
        }
    }
}