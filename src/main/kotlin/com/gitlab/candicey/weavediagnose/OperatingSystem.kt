package com.gitlab.candicey.weavediagnose

enum class OperatingSystem {
    WINDOWS,
    LINUX,
    MACOS,
    UNKNOWN;

    companion object {
        fun current(): OperatingSystem {
            val os = System.getProperty("os.name").lowercase()
            return when {
                os.contains("win") -> WINDOWS
                os.contains("nux") -> LINUX
                os.contains("mac") -> MACOS
                else -> UNKNOWN
            }
        }
    }
}