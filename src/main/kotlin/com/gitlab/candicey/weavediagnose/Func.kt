package com.gitlab.candicey.weavediagnose

import javax.swing.JOptionPane

fun showError(message: String) {
    JOptionPane.showMessageDialog(null, message, "Weave Diagnose | Error", JOptionPane.ERROR_MESSAGE)
}