package com.gitlab.candicey.weavediagnose

import kotlinx.serialization.Serializable

@Serializable
data class Config(
    var minMemory: Int = 1536,
    var maxMemory: Int = 1536,
) {
    companion object {
        lateinit var INSTANCE: Config

        init {
            load()
        }

        fun save() {
            CONFIG_JSON_FILE.createNewFile()
            CONFIG_JSON_FILE.writeText(JSON.encodeToString(serializer(), INSTANCE))
        }

        fun load() {
            if (CONFIG_JSON_FILE.exists()) {
                INSTANCE = JSON.decodeFromString(serializer(), CONFIG_JSON_FILE.readText())
            } else {
                INSTANCE = Config()
                save()
            }
        }
    }
}