package com.gitlab.candicey.weavediagnose

import java.awt.Desktop
import java.io.File
import javax.swing.JFrame
import javax.swing.JOptionPane
import kotlin.system.exitProcess

fun main(args: Array<String>) {
    if (args.isEmpty()) {
        isGui = true

        val options = arrayOf("Run Weave", "Update Weave", "Open Mods Directory", "Clear Lunar Cache", "Open Logs", "Settings")
        val choice = JOptionPane.showOptionDialog(null, "What would you like to do?", "Weave Diagnose", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0])
        when (choice) {
            0 -> {
                runProcess(emptyList(), true)
            }

            1 -> {
                WeaveHandler.updateJar()
            }

            2 -> {
                Desktop.getDesktop().open(MODS_DIRECTORY)
            }

            3 -> {
                val cacheDirectory = File(MULTIVER_DIRECTORY, "cache")
                cacheDirectory.deleteRecursively()
            }

            4 -> {
                if (!LOGS_FILE.exists()) {
                    JOptionPane.showMessageDialog(null, "No logs found.")
                    exitProcess(0)
                }

                Desktop.getDesktop().open(LOGS_DIRECTORY)
            }

            5 -> {
                val settingsOptions = arrayOf("Minimum Memory", "Maximum Memory")
                val settingsChoice = JOptionPane.showOptionDialog(null, "What would you like to do?", "Weave Diagnose", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, settingsOptions, settingsOptions[0])
                when (settingsChoice) {
                    0 -> {
                        val minMemory = JOptionPane.showInputDialog("Enter the minimum amount of memory to allocate to the JVM in MB (default: 1536): ", Config.INSTANCE.minMemory)
                        if (minMemory != null) {
                            Config.INSTANCE.minMemory = minMemory.toIntOrNull() ?: run {
                                JOptionPane.showMessageDialog(null, "Invalid number.")
                                exitProcess(0)
                            }
                            Config.save()
                        }
                    }

                    1 -> {
                        val maxMemory = JOptionPane.showInputDialog("Enter the maximum amount of memory to allocate to the JVM in MB (default: 1536): ", Config.INSTANCE.maxMemory)
                        if (maxMemory != null) {
                            Config.INSTANCE.maxMemory = maxMemory.toIntOrNull() ?: run {
                                JOptionPane.showMessageDialog(null, "Invalid number.")
                                exitProcess(0)
                            }
                            Config.save()
                        }
                    }
                }
            }
        }

        exitProcess(0)
    }

    when (args[0]) {
        "update" -> {
            WeaveHandler.updateJar()
        }

        "run" -> {
            val secondArg = if (args.size > 1) {
                args[1]
            } else {
                println("Valid arguments are: weave, normal")
                exitProcess(0)
            }

            val additionalArguments = if (args.size > 2) {
                args.sliceArray(2 until args.size).toList()
            } else {
                emptyList()
            }

            when (secondArg) {
                "weave" -> {
                    runProcess(additionalArguments, true)
                }

                "normal" -> {
                    runProcess(additionalArguments)
                }

                else -> {
                    println("Invalid argument. Valid arguments are: weave, normal")
                }
            }
        }

        "print" -> {
            val secondArg = if (args.size > 1) {
                args[1]
            } else {
                println("Valid arguments are: weave, normal")
                exitProcess(0)
            }

            when (secondArg) {
                "weave" -> {
                    println(CommandBuilder.buildCommand().joinToString(" "))
                }

                "normal" -> {
                    println(CommandBuilder.buildCommand(withWeave = false).joinToString(" "))
                }

                else -> {
                    println("Invalid argument. Valid arguments are: weave, normal")
                }
            }
        }

        "copy" -> {
            val secondArg = if (args.size > 1) {
                args[1]
            } else {
                println("Valid arguments are: weave, normal")
                exitProcess(0)
            }

            when (secondArg) {
                "weave" -> {
                    ClipboardHelper.setClipboardString(CommandBuilder.buildCommand().joinToString(" "))
                }

                "normal" -> {
                    ClipboardHelper.setClipboardString(CommandBuilder.buildCommand(withWeave = false).joinToString(" "))
                }

                else -> {
                    println("Invalid argument. Valid arguments are: weave, normal")
                }
            }
        }

        "find" -> {
            val copyOrPrint = { argsList: Array<String>, targetIndex: Int, string: String ->
                if (argsList.size > targetIndex && argsList[targetIndex] == "copy") {
                    ClipboardHelper.setClipboardString(string)
                } else {
                    println(string)
                }
            }

            when (if (args.size > 1) args[1] else "help") {
                "weave" -> {
                    copyOrPrint(args, 2, CommandBuilder.getWeaveJarOrExit().canonicalPath)
                }

                "minecraft" -> {
                    copyOrPrint(args, 2, CommandBuilder.findMinecraftDirectory().canonicalPath)
                }

                "java" -> {
                    copyOrPrint(args, 2, CommandBuilder.findJava().canonicalPath)
                }

                "help" -> {
                    println("Valid arguments are: weave, minecraft, java")
                }

                else -> {
                    println("Invalid argument. Valid arguments are: weave, minecraft, java")
                }
            }
        }

        "batch" -> {
            if (OperatingSystem.current() != OperatingSystem.WINDOWS) {
                System.err.println("Batch script is only available on Windows.")
                exitProcess(0)
            }

            val additionalArguments = if (args.size > 1) {
                args.sliceArray(1 until args.size).toList()
            } else {
                emptyList()
            }

            generateBatchScript(additionalArguments)
        }

        "help" -> {
            println("Valid commands are: update, run, print, copy, find, logs, help")
            println("Available system properties:")
            println("- minecraft.directory: Minecraft directory")
            println("- memory.minimum: Minimum memory (default: 1536m)")
            println("- memory.maximum: Maximum memory (default: 1536m)")
        }

        "logs" -> {
            if (!LOGS_FILE.exists()) {
                println("No logs found.")
                exitProcess(0)
            }

            Desktop.getDesktop().open(LOGS_DIRECTORY)
        }

        else -> {
            println("Invalid command. Valid commands are: update, run, print, copy, find, logs, help")
        }
    }
}

fun runProcess(additionalArguments: List<String>, withWeave: Boolean = false) {
    WeaveHandler.checkMods()

    // clear logs
    LOGS_DIRECTORY.delete()
    LOGS_DIRECTORY.mkdirs()

    LOGS_FILE.createNewFile()
    LOGS_FILE.writeText("")

    val stdoutAppend: (String) -> Unit = {
        LOGS_FILE.appendText("$it\n")
        println(it)
    }
    val stderrAppend: (String) -> Unit = {
        LOGS_FILE.appendText("$it\n")
        System.err.println(it)
    }

    val command = CommandBuilder.buildCommand(additionalArguments, withWeave)

    stdoutAppend("Command: ${command.joinToString(" ")}")
    stdoutAppend("Working directory: ${MULTIVER_DIRECTORY.canonicalPath}")
    stdoutAppend("Config: ${Config.INSTANCE}")
    stdoutAppend("")

    ProcessRunner.runCommand(
        command,
        MULTIVER_DIRECTORY,
        stdoutAppend,
        stderrAppend
    )
}