package com.gitlab.candicey.weavediagnose

import java.io.File

fun generateBatchScript(additionalArguments: List<String>) {
    val currentJarPath = File(WeaveHandler::class.java.protectionDomain.codeSource.location.toURI()).canonicalPath

    val prompt = prompt@ { message: String, defaultValue: String? ->
        while (true) {
            print(message)

            val line = readlnOrNull() ?: continue

            if (line.isBlank()) {
                if (defaultValue != null) {
                    return@prompt defaultValue
                } else {
                    continue
                }
            }

            if (line == "exit") {
                return@prompt null
            } else {
                return@prompt line
            }
        }
    }

    val minMemory = prompt("Enter the minimum amount of memory to allocate to the JVM in MB (default: 1536): ", "1536")?.toString()?.toIntOrNull() ?: return
    val maxMemory = prompt("Enter the maximum amount of memory to allocate to the JVM in MB (default: 1536): ", "1536")?.toString()?.toIntOrNull() ?: return
    val minecraftDirectory = CommandBuilder.findMinecraftDirectory().canonicalPath.let{ prompt("Enter the Minecraft directory (default: $it): ", it) ?: return }
    val javaPath = CommandBuilder.findJava().canonicalPath.let{ prompt("Enter the path to Java executable (default: $it): ", it) ?: return }

    val script = """
        @echo off
        
        $javaPath -jar "$currentJarPath" update
        
        $javaPath -Dminecraft.directory="$minecraftDirectory" -Dmemory.minimum="${minMemory}m" -Dmemory.maximum="${maxMemory}m" -jar "$currentJarPath" run weave ${additionalArguments.joinToString(" ")}
        pause
    """.trimIndent()

    File("run_weave.bat")
        .also { it.createNewFile() }
        .writeText(script)
}