package com.gitlab.candicey.weavediagnose

import java.awt.Image
import java.awt.Toolkit
import java.awt.datatransfer.*

object ClipboardHelper {
    val systemClipboard: Clipboard = Toolkit.getDefaultToolkit().systemClipboard

    fun setClipboardString(string: String) = systemClipboard.setContents(StringSelection(string), null)

    fun setClipboardImage(image: Image) = systemClipboard.setContents(ImageTransferable(image), null)

    class ImageTransferable(private val image: Image) : Transferable {
        override fun getTransferDataFlavors(): Array<DataFlavor> = arrayOf(DataFlavor.imageFlavor)

        override fun isDataFlavorSupported(flavour: DataFlavor?): Boolean = transferDataFlavors.contains(flavour)

        override fun getTransferData(flavour: DataFlavor?): Any {
            if (flavour == null || !isDataFlavorSupported(flavour)) {
                throw UnsupportedFlavorException(flavour)
            }

            return image
        }
    }
}