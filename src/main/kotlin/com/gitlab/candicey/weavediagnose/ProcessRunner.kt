package com.gitlab.candicey.weavediagnose

import java.io.File

object ProcessRunner {
    fun runCommand(command: List<String>, workingDirectory: File, stdout: (String) -> Unit, stderr: (String) -> Unit) {
        val process = ProcessBuilder(command)
            .directory(workingDirectory)
            .redirectOutput(ProcessBuilder.Redirect.PIPE)
            .redirectError(ProcessBuilder.Redirect.PIPE)
            .start()

        val stdoutThread = Thread {
            process.inputStream.bufferedReader().forEachLine(stdout)
        }
        val stderrThread = Thread {
            process.errorStream.bufferedReader().forEachLine(stderr)
        }

        stdoutThread.start()
        stderrThread.start()

        process.waitFor()
        stdoutThread.join()
        stderrThread.join()

        LoggingHandler.onGameExit(process.exitValue())
    }
}