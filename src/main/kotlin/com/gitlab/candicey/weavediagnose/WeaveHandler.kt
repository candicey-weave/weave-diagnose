package com.gitlab.candicey.weavediagnose

import java.io.File
import java.net.HttpURLConnection
import java.net.URL
import java.util.zip.ZipFile
import javax.swing.JOptionPane

object WeaveHandler {
    init {
        WEAVE_DIRECTORY.mkdirs()
    }

    private val weaveJarRegex = Regex("Weave-Loader-v\\d+\\.\\d+\\.\\d+\\.jar")

    private fun fetchApi(): Pair<String, String> {
        val url = URL("https://api.github.com/repos/weave-mc/weave-loader/releases")
        val httpURLConnection = url.openConnection() as HttpURLConnection
        val response = httpURLConnection.inputStream.bufferedReader().readText()
        val json = JSON_PARSER.parse(response).asJsonArray
        val latestRelease = json[0].asJsonObject
        val latestVersion = latestRelease["tag_name"].asString
        val downloadUrl = latestRelease["assets"].asJsonArray[0].asJsonObject["browser_download_url"].asString
        return Pair(latestVersion, downloadUrl)
    }

    private fun downloadWeave(latestReleaseVersion: String, downloadUrl: String) {
        val jarFile = File(WEAVE_DIRECTORY, "Weave-Loader-$latestReleaseVersion.jar")
        if (jarFile.exists()) {
            jarFile.delete()
        }
        jarFile.createNewFile()
        val url = URL(downloadUrl)
        val httpURLConnection = url.openConnection() as HttpURLConnection
        val inputStream = httpURLConnection.inputStream
        val outputStream = jarFile.outputStream()
        inputStream.copyTo(outputStream)
        LoggingHandler.info("Downloaded Weave jar: ${jarFile.canonicalPath}")
    }

    fun listJar(): List<File> = WEAVE_DIRECTORY.listFiles { _, s -> s.endsWith(".jar") }?.toList() ?: emptyList()

    fun updateJar(promptUpdate: Boolean = false) {
        val (latestReleaseVersion, downloadUrl) = fetchApi()

        val jars = listJar()
        if (jars.isEmpty()) {
            downloadWeave(latestReleaseVersion, downloadUrl)
        } else {
            var latestJarVersion = ""
            var latestJarFile: File? = null
            jars.forEach {
                val version = weaveJarRegex.find(it.name)?.value?.removePrefix("Weave-Loader-")?.removeSuffix(".jar") ?: ""
                if (version > latestJarVersion) {
                    latestJarVersion = version
                    latestJarFile = it
                }
            }

            if (latestJarFile == null) {
                downloadWeave(latestReleaseVersion, downloadUrl)
            } else {
                println("Found Weave jar: ${latestJarFile!!.canonicalPath}")

                if (latestReleaseVersion != latestJarVersion) {
                    if (promptUpdate) {
                        val update = JOptionPane.showConfirmDialog(null, "Do you want to update Weave?", "Weave Diagnose", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION
                        if (!update) {
                            return
                        }
                    }

                    println("Latest Weave version: $latestReleaseVersion")
                    println("Latest Weave jar version: $latestJarVersion")
                    println("Downloading latest Weave jar...")
                    downloadWeave(latestReleaseVersion, downloadUrl)
                } else {
                    LoggingHandler.info("Weave is up to date.")
                }
            }
        }
    }

    fun getJar(): File? {
        val jars = listJar()
        return if (jars.isEmpty()) {
            null
        } else {
            var latestJarVersion = ""
            var latestJarFile: File? = null
            jars.forEach {
                val version = weaveJarRegex.find(it.name)?.value?.removePrefix("Weave-Loader-v") ?: ""
                if (version > latestJarVersion) {
                    latestJarVersion = version
                    latestJarFile = it
                }
            }

            latestJarFile
        }
    }

    fun checkMods() {
        if (!isGui) {
            return
        }

        val jars = MODS_DIRECTORY.listFiles { _, s -> s.endsWith(".jar") }?.toList() ?: return
        val nonWeaveJars = jars.filter { jar -> runCatching { ZipFile(jar).getEntry("weave.mod.json") == null }.getOrElse { true } }
        if (nonWeaveJars.isNotEmpty()) {
            LoggingHandler.error("Found non-Weave mods: ${nonWeaveJars.joinToString(", ") { it.name }}. (Weave Loader only supports Weave mods.)")
        }
    }
}