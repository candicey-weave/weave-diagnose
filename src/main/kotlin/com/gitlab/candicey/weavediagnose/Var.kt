package com.gitlab.candicey.weavediagnose

import com.google.gson.JsonParser
import kotlinx.serialization.json.Json
import java.io.File

val HOME: String = System.getProperty("user.home")

val MULTIVER_DIRECTORY: File = File(HOME, ".lunarclient/offline/multiver")

val WEAVE_DIRECTORY: File = File(HOME, ".weave").alsoMkdirs()

val MODS_DIRECTORY: File = File(WEAVE_DIRECTORY, "mods").alsoMkdirs()

val CONFIG_DIRECTORY: File = File(WEAVE_DIRECTORY, "weavediagnose").alsoMkdirs()
val CONFIG_JSON_FILE: File = File(CONFIG_DIRECTORY, "config.json")

val LOGS_DIRECTORY: File = File(CONFIG_DIRECTORY, "logs")
val LOGS_FILE: File = File(LOGS_DIRECTORY, "latest.log")

val JSON_PARSER = JsonParser()

val JSON = Json {
    prettyPrint = true
    ignoreUnknownKeys = true
}

var isGui: Boolean = false