package com.gitlab.candicey.weavediagnose

import java.io.File

fun File.alsoMkdirs(): File {
    mkdirs()
    return this
}